import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

public class Temp_Servlet_2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//  String one = req.getParameter("one");
//  String two = req.getParameter("two");
//  String abc = req.getParameter("abc");
// System.out.println(one+" "+two+" "+abc);
/* String[] ones = req.getParameterValues("one");
     for(String s : ones ) {
        System.out.println(s);
     }
*/
//    Enumeration<String> parameterNames = req.getParameterNames();
//       while(parameterNames.hasMoreElements()) {
//           String elementName = parameterNames.nextElement();
//           System.out.println(elementName + "=" + req.getParameter(elementName));
//       }

//      Map<String, String[]> parametrMap = req.getParameterMap();
//        System.out.println(req.getRequestURL());
//        System.out.println(req.getServletPath());
//        System.out.println(req.getRemoteHost());
//        System.out.println(req.getRequestURI());
//        System.out.println(req.getLocalPort());
//        System.out.println(req.getQueryString());

        // Формочка
        String one = req.getParameter("one");
        one = one == null ? "" : one.replaceAll("<", "&lt;").replaceAll(">","&gt;");
        resp.getWriter().write("<html>" +
                "<head></head>" +
                "<body>" +
                "one = " + one +
                "<form action = 'temp' method='post'>" +
                "<textarea name='one'></textarea>" +
                "<input type='submit' name='submit'/>" +
                "</form>" +
                "</body>" +
                "</html>");
 }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doGet(req,resp);
    }

}

